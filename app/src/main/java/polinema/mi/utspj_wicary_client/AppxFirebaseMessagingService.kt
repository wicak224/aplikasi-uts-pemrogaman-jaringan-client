package polinema.mi.utspj_wicary_client

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class AppxFirebaseMessagingService : FirebaseMessagingService() {

    var alamatTujuan = ""
    var linkGambar = ""
    var pesanJarkom = ""
    var body = ""
    var title = ""
    var RC_INTENT = 100
    var CHANNEL_ID = "appx0f"

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
    }

    override fun onMessageReceived(p0: RemoteMessage?) {
        super.onMessageReceived(p0)

        val intent = Intent(this, MainActivityMap::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        if (p0!!.getData().size!! > 0){
            alamatTujuan = p0.data.getValue("alamatTujuan")
            linkGambar = p0.data.getValue("linkGambar")
            pesanJarkom = p0.data.getValue("pesanJarkom")

            intent.putExtra("alamatTujuan",alamatTujuan)
            intent.putExtra("linkGambar",linkGambar)
            intent.putExtra("pesanJarkom",pesanJarkom)
            intent.putExtra("type",0)

            sendNotif("JARKOM!!!!!!!" , " $pesanJarkom",intent) //send notifikasi, intent untuk mengirimkan data
        }

        if (p0.notification != null){
            body = p0.notification!!.body!!
            title = p0.notification!!.title!!

            intent.putExtra("title",title)
            intent.putExtra("body",body)
            intent.putExtra("type",1)

            sendNotif(title,body, intent)
        }
    }
    fun sendNotif(title: String, body : String, inten : Intent){
        val pendingIntent = PendingIntent.getActivity(this,RC_INTENT,inten,PendingIntent.FLAG_ONE_SHOT)

        val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        val audioAttributes = AudioAttributes.Builder().
                setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE).
                setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build()
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val mChannel = NotificationChannel(CHANNEL_ID,"apx0f",
            NotificationManager.IMPORTANCE_HIGH)
            mChannel.description = "App x0f"
            mChannel.setSound(ringtoneUri,audioAttributes)
            notificationManager.createNotificationChannel(mChannel)
        }

        val notificationBuilder = NotificationCompat.Builder(baseContext,CHANNEL_ID)
            .setSmallIcon(R.drawable.icons8_error_32px)
            .setLargeIcon(BitmapFactory.decodeResource(resources,R.drawable.icons8_spam_96px_1))
            .setContentTitle(title)
            .setContentText(body)
            .setSound(ringtoneUri)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true).build()
        notificationManager.notify(RC_INTENT, notificationBuilder)
    }
}