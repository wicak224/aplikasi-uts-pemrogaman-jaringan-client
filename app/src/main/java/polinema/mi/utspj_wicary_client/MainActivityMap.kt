package polinema.mi.utspj_wicary_client

import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_map.*
import mumayank.com.airlocationlibrary.AirLocation
import java.lang.Exception

class MainActivityMap : AppCompatActivity(), View.OnClickListener, OnMapReadyCallback{


    var bundle : Bundle? = null
    var topik = "appx0f"
    var type = 0

    var PARE = "pare"
    var WATES = "wates+jawa+timur"
    val MAPBOX_TOKEN ="pk.eyJ1Ijoid2ljYWsyMjQiLCJhIjoiY2tnNXp5Z3NtMHlybzJzbHdzMjl0c3Q3MyJ9.a7rA4N5MjoNS5xoF0zw93Q"
    var URL = ""
    var linkGbr = ""
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnGambar ->{
                var lokasi = Intent(Intent.ACTION_VIEW, Uri.parse(linkGbr))
                startActivity(lokasi)
            }

            R.id.fab -> {
                val ll = LatLng(lat,lng)
                gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))

            }
        }
    }

    fun getDestinationLocation(url : String){
        val request = JsonObjectRequest(Request.Method.GET,url,null,
            Response.Listener {
                val features = it.getJSONArray("features").getJSONObject(0)
                val place_name = features.getString("place_name")
                val center = features.getJSONArray("center")
                val lat = center.get(0).toString()
                val lng = center.get(1).toString()
                getDestinationRoutes(lng,lat,place_name)
            }, Response.ErrorListener {
                Toast.makeText(this,"Can't get Destination Location",Toast.LENGTH_SHORT).show()
            })
        val q = Volley.newRequestQueue(this)
        q.add(request)
    }

    fun getDestinationRoutes(destLat : String, destLng : String, place_name : String){
        URL = "https://api.mapbox.com/directions/v5/mapbox/driving/" +
                "$lng,$lat;$destLng,$destLat?access_token=$MAPBOX_TOKEN&geometries=geojson"
        val request = JsonObjectRequest(Request.Method.GET,URL, null,
            Response.Listener {
                val routes = it.getJSONArray("routes").getJSONObject(0)
                val legs = routes.getJSONArray("legs").getJSONObject(0)
                val distance = legs.getInt("distance")/1000.0
                val duration = legs.getInt("duration")/60
                txMyLocation.setText("My Location :\nLat : $lat Lng : $lng\n" +
                        "Destination : $place_name\n" +
                        "Distance : $distance km\n Duration : $duration minute")
                val geometry = routes.getJSONObject("geometry")
                val coordinates = geometry.getJSONArray("coordinates")
                var arraySteps = ArrayList<LatLng>()
                for (i in 0..coordinates.length()-1){
                    val lngLat = coordinates.getJSONArray(i)
                    val fLng = lngLat.getDouble(0)
                    val fLat = lngLat.getDouble(1)
                    arraySteps.add(LatLng(fLat,fLng))
                }
                drawRoutes(arraySteps,place_name)
            },
            Response.ErrorListener {
                Toast.makeText(this,"something is wrong! ${it.message.toString()}",
                    Toast.LENGTH_SHORT).show()
            })
        val q = Volley.newRequestQueue(this)
        q.add(request)
    }

    fun drawRoutes(array: ArrayList<LatLng>,place_name: String){
        gMap?.clear()
        val polyline = PolylineOptions().color(Color.BLUE).width(10.0f)
            .clickable(true).addAll(array)
        gMap?.addPolyline(polyline)
        val ll = LatLng(lat,lng)
        gMap?.addMarker(MarkerOptions().position(ll).title("Hei! I'am here"))
        gMap?.addMarker(MarkerOptions().position(array.get(array.size-1)).title(place_name))
        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,10.0f))
    }

    override fun onMapReady(p0: GoogleMap?) {
        gMap = p0
        if (gMap !=null){
            airLoc = AirLocation(this,true,true,
                object : AirLocation.Callbacks{
                    override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                        Toast.makeText(this@MainActivityMap,"Failed to get current location",
                            Toast.LENGTH_SHORT).show()
                        txMyLocation.setText("Failed to get current location")
                    }

                    override fun onSuccess(location: Location) {
                        lat = location.latitude; lng = location.longitude
                        val ll = LatLng(location.latitude,location.longitude)
                        gMap!!.addMarker(MarkerOptions().position(ll).title("Hei! I'm Here Bro"))
                        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                        txMyLocation.setText("My Position : LAT=${location.latitude}, "+
                                "LNG=${location.longitude}")
                    }
                })
        }
    }

    var lat : Double = 0.0; var lng : Double = 0.0;
    var airLoc : AirLocation? = null
    var gMap : GoogleMap? = null
    lateinit var mapFragment : SupportMapFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_map)
        mapFragment = supportFragmentManager.findFragmentById(R.id.mapsFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        btnGambar.setOnClickListener(this)
        fab.setOnClickListener(this)
        FirebaseMessaging.getInstance().subscribeToTopic(topik)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        airLoc?.onActivityResult(requestCode,resultCode,data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        airLoc?.onRequestPermissionsResult(requestCode,permissions,grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onResume() {
        super.onResume()

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(
            OnCompleteListener { task ->
                if (!task.isSuccessful)return@OnCompleteListener
//                edToken.setText(task.result!!.token)
            }
        )

        try {
            bundle = getIntent().getExtras()!!
        }catch (e : Exception){
            Log.e("BUNDLE","bundle is null")
        }

        if(bundle != null){
            type = bundle!!.getInt("type")
            when(type){
                0 ->{
                    var tujuan = bundle!!.getString("alamatTujuan").toString().replace(" ","+")
                    var pesanJarkom = bundle!!.getString("pesanJarkom").toString()
                    linkGbr = bundle!!.getString("linkGambar").toString()
                    URL = "https://api.mapbox.com/geocoding/v5/mapbox.places/" +
                            "$tujuan.json?proximity=$lng,$lat&access_token=$MAPBOX_TOKEN&limit=1"
                    getDestinationLocation(URL)
                    txJarkom.setText(pesanJarkom)
                    WATES = bundle!!.getString("promo").toString()
                }
                1 -> {
                    edTitle.setText(bundle!!.getString("title"))
                    edBody.setText(bundle!!.getString("body"))
                }
            }
        }
    }

}