package polinema.mi.utspj_wicary_client

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_map.*
import java.lang.Exception
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var bundle : Bundle? = null
    var topik = "appx0f"
    var type = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnGambar.setOnClickListener(this)
        FirebaseMessaging.getInstance().subscribeToTopic(topik)
    }

    override fun onResume() {
        super.onResume()

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(
            OnCompleteListener { task ->
                if (!task.isSuccessful)return@OnCompleteListener
                edToken.setText(task.result!!.token)
            }
        )

        try {
            bundle = getIntent().getExtras()!!
        }catch (e : Exception){
            Log.e("BUNDLE","bundle is null")
        }

        if(bundle != null){
            type = bundle!!.getInt("type")
            when(type){
                0 ->{
                    edPromoId.setText(bundle!!.getString("promoID"))
                    edPromo.setText(bundle!!.getString("promo"))
                    edPromoUntil.setText(bundle!!.getString("promoUntil"))
                }
                1 -> {
                    edTitle.setText(bundle!!.getString("title"))
                    edBody.setText(bundle!!.getString("body"))
                }
            }
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnGambar ->{
                var gambar = "https://www.youtube.com/"
                var lokasi = Intent(Intent.ACTION_VIEW,Uri.parse(gambar))
                startActivity(lokasi)
            }
        }
    }
}